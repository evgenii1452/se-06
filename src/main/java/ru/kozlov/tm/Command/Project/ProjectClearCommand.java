package ru.kozlov.tm.Command.Project;

import ru.kozlov.tm.Command.AbstractCommand;
import ru.kozlov.tm.Const.Command;
import ru.kozlov.tm.Enum.RoleType;

public class ProjectClearCommand extends AbstractCommand {
    @Override
    public RoleType[] roleTypes() {
        return new RoleType[] {RoleType.ADMIN, RoleType.CUSTOMER};
    }

    @Override
    public String getName() {
        return Command.PROJECT_CLEAR;
    }

    @Override
    public String getDescription() {
        return "Remove all projects";
    }

    @Override
    public void execute() {
        String userId = bootstrap.getUserId();
        bootstrap.getProjectController().removeAll(userId);

        System.out.println("[ВСЕ ПРОЕКТЫ УДАЛЕНЫ]");
    }
}
