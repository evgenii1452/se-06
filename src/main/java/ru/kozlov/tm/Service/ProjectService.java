package ru.kozlov.tm.Service;

import ru.kozlov.tm.Entity.Project;
import ru.kozlov.tm.Repository.ProjectRepository;
import ru.kozlov.tm.Util.HelpUtil;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class ProjectService {
    private final ProjectRepository projectRepository;
    private final TaskService taskService;

    public ProjectService(
            final ProjectRepository projectRepository,
            final TaskService taskService
    ) {
        this.projectRepository = projectRepository;
        this.taskService = taskService;
    }

    public void removeAllProjects(final String userId) {
        projectRepository.removeAll(userId);
    }

    public String createProject(
            final String userId,
            final String name,
            final String description,
            final String dateStartAsString,
            final String dateEndAsString
    ) throws ParseException {
        final String id = UUID.randomUUID().toString();

        stringValidation(name);
        stringValidation(description);
        stringValidation(dateStartAsString);
        stringValidation(dateEndAsString);

        final Date dateStart = HelpUtil.convertStringToDate(dateStartAsString);
        final Date dateEnd = HelpUtil.convertStringToDate(dateEndAsString);

        final Project project = new Project();
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        project.setDateStart(dateStart);
        project.setDateEnd(dateEnd);
        project.setUserId(userId);

        return projectRepository.persist(project);
    }

    public List<Project> getAllProjects(final String userId) {
        return projectRepository.findAll(userId);
    }

    public void removeProjectById(final String userId, final String projectId) throws Exception {
        stringValidation(projectId);
        checkProjectExists(projectId);

        if (isProjectBelongsToUser(projectId, userId)) {
            throw new Exception("[ДОСТУП ЗАПРЕЩЕН]");
        }

        taskService.removeTasksByProjectId(userId, projectId);
        projectRepository.remove(projectId);

    }

    public Project getProjectById(final String id) {
        stringValidation(id);
        checkProjectExists(id);

        return projectRepository.find(id);
    }

    public void updateProject(
            final String projectId,
            final String name,
            final String description,
            final String dateStartAsString,
            final String dateEndAsString,
            final String userId
    ) throws Exception {
        stringValidation(projectId);
        stringValidation(name);
        stringValidation(description);
        stringValidation(dateStartAsString);
        stringValidation(dateEndAsString);

        if (isProjectBelongsToUser(projectId, userId)) {
            throw new Exception("[ДОСТУП ЗАПРЕЩЕН]");
        }
        final Date dateStart = HelpUtil.convertStringToDate(dateStartAsString);
        final Date dateEnd = HelpUtil.convertStringToDate(dateEndAsString);

        final Project project = new Project();
        project.setId(projectId);
        project.setName(name);
        project.setDescription(description);
        project.setDateStart(dateStart);
        project.setDateEnd(dateEnd);

        projectRepository.merge(project);
    }

    private void checkProjectExists(final String id) {
        if (!projectRepository.keyExists(id)) {
            throw new IllegalArgumentException("[ПРОЕКТ НЕ НАЙДЕН]");
        }
    }

    private void stringValidation(final String value) {
        if (HelpUtil.stringIsEmpty(value)) {
            throw new NullPointerException("[ВСЕ ПОЛЯ ДОЛЖНЫ БЫТЬ ЗАПОЛНЕНЫ]");
        }
    }

    public boolean isProjectBelongsToUser(final String projectId, final String userId) {
        Project project = getProjectById(projectId);

        return userId.equals(project.getUserId());
    }
}
