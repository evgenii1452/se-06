package ru.kozlov.tm.Enum;

public enum RoleType {
    ADMIN("Админ"), CUSTOMER("Пользователь");

    private final String type;

    RoleType(String type) {
        this.type = type;
    }

    public String displayName() {
        return type;
    }
}
