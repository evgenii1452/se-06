package ru.kozlov.tm.Command.User;

import ru.kozlov.tm.Command.AbstractCommand;
import ru.kozlov.tm.Entity.User;
import ru.kozlov.tm.Enum.RoleType;

public class UserLogoutCommand extends AbstractCommand {
    @Override
    public RoleType[] roleTypes() {
        return new RoleType[] {RoleType.ADMIN, RoleType.CUSTOMER};
    }

    @Override
    public String getName() {
        return "user-logout";
    }

    @Override
    public String getDescription() {
        return "logout";
    }

    @Override
    public void execute() {
        bootstrap.setCurrentUser(new User());
    }
}
