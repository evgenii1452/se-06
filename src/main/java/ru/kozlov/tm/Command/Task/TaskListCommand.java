package ru.kozlov.tm.Command.Task;

import ru.kozlov.tm.Command.AbstractCommand;
import ru.kozlov.tm.Const.Command;
import ru.kozlov.tm.Enum.RoleType;

public class TaskListCommand extends AbstractCommand {
    @Override
    public RoleType[] roleTypes() {
        return new RoleType[] {RoleType.ADMIN, RoleType.CUSTOMER};
    }

    @Override
    public String getName() {
        return Command.TASK_LIST;
    }

    @Override
    public String getDescription() {
        return "show all tasks";
    }

    @Override
    public void execute() {
        final String userId = bootstrap.getUserId();
        bootstrap.getProjectController().showAll(userId);

        System.out.println("Введите id проекта:");
        final String  projectId = bootstrap.getScanner().nextLine();

        bootstrap.getTaskController().showAll(userId, projectId);
    }
}
