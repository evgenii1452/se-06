package ru.kozlov.tm.Command.User;

import ru.kozlov.tm.Command.AbstractCommand;
import ru.kozlov.tm.Entity.User;
import ru.kozlov.tm.Enum.RoleType;

public class UserLoginCommand extends AbstractCommand {
    @Override
    public RoleType[] roleTypes() {
        return new RoleType[] {null};
    }

    @Override
    public String getName() {
        return "user-login";
    }

    @Override
    public String getDescription() {
        return "Log in to task manager";
    }

    @Override
    public void execute() {
        System.out.println("Введите логин:");
        String login = bootstrap.getScanner().nextLine();
        System.out.println("Введите пароль:");
        String password = bootstrap.getScanner().nextLine();

        User user = bootstrap.getUserController().authentication(login, password);

        if (user != null) {
            bootstrap.setCurrentUser(user);
            System.out.println("[ВЫПОЛНЕН ВХОД В СИСТЕМУ]");
        }
    }
}
