package ru.kozlov.tm.Command.User;

import ru.kozlov.tm.Command.AbstractCommand;
import ru.kozlov.tm.Enum.RoleType;

public class UserRegistrationCommand extends AbstractCommand {
    @Override
    public RoleType[] roleTypes() {
        return new RoleType[] {null};
    }

    @Override
    public String getName() {
        return "user-registration";
    }

    @Override
    public String getDescription() {
        return "Registration";
    }

    @Override
    public void execute() {
        System.out.println("Введите логин:");
        String login = bootstrap.getScanner().nextLine();
        System.out.println("Введите пароль:");
        String password = bootstrap.getScanner().nextLine();

        bootstrap.getUserController().registration(login, password);
    }
}
