package ru.kozlov.tm.Command.User;

import ru.kozlov.tm.Command.AbstractCommand;
import ru.kozlov.tm.Enum.RoleType;

public class UserUpdatePasswordCommand extends AbstractCommand {
    @Override
    public RoleType[] roleTypes() {
        return new RoleType[] {RoleType.ADMIN, RoleType.CUSTOMER};
    }

    @Override
    public String getName() {
        return "user-update-pswd";
    }

    @Override
    public String getDescription() {
        return "Update password";
    }

    @Override
    public void execute() {
        String login = bootstrap.getCurrentUser().getLogin();
        System.out.println("Введите текущий пароль:");
        final String currentPassword = bootstrap.getScanner().nextLine();

        System.out.println("Введите новый пароль:");
        final String newPassword = bootstrap.getScanner().nextLine();

        bootstrap.getUserController().updatePassword(login, currentPassword, newPassword);
    }
}
