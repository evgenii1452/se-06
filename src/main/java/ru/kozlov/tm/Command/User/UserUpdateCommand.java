package ru.kozlov.tm.Command.User;

import ru.kozlov.tm.Command.AbstractCommand;
import ru.kozlov.tm.Enum.RoleType;

public class UserUpdateCommand extends AbstractCommand {
    @Override
    public RoleType[] roleTypes() {
        return new RoleType[] {RoleType.ADMIN, RoleType.CUSTOMER};
    }

    @Override
    public String getName() {
        return "user-update";
    }

    @Override
    public String getDescription() {
        return "Update user info";
    }

    @Override
    public void execute() {
        final String id = bootstrap.getUserId();
        System.out.println("Введите логин:");
        final String newLogin = bootstrap.getScanner().nextLine();
        System.out.println("Введите пароль:");
        final String newPassword = bootstrap.getScanner().nextLine();

        bootstrap.getUserController().update(id, newLogin, newPassword);
    }
}
