package ru.kozlov.tm.Repository;

import ru.kozlov.tm.Entity.User;

import java.util.HashMap;
import java.util.Map;

public class UserRepository {
    private final Map<String, User> users = new HashMap<>();

    public User findByLogin(final String login) {
        for (Map.Entry<String, User> userEntry: users.entrySet()) {
            if (userEntry.getValue().getLogin().equals(login)) {
                return userEntry.getValue();
            }
        }

        return null;
    }

    public User persist(final User user) {
        return users.put(user.getId(), user);
    }

    public void merge(final User user) {
        if (users.containsKey(user.getId())) {
            users.replace(user.getId(), user);
        }

        if (!users.containsKey(user.getId())) {
            users.put(user.getId(), user);
        }
    }

    public boolean loginExists(final String login) {
        for (Map.Entry<String, User> userMap: users.entrySet()) {
            if (login.equals(userMap.getValue().getLogin())) {
                return true;
            }
        }

        return false;
    }

    public User getById(final String id) {
        return users.get(id);
    }
}
