package ru.kozlov.tm.Command.Task;

import ru.kozlov.tm.Command.AbstractCommand;
import ru.kozlov.tm.Const.Command;
import ru.kozlov.tm.Enum.RoleType;

public class TaskGenerateCommand extends AbstractCommand {
    @Override
    public RoleType[] roleTypes() {
        return new RoleType[] {RoleType.ADMIN, RoleType.CUSTOMER};
    }

    @Override
    public String getName() {
        return Command.TASK_GENERATE;
    }

    @Override
    public String getDescription() {
        return "Task generation";
    }

    @Override
    public void execute() {
        final String userId = bootstrap.getUserId();

        bootstrap.getTaskController().generate(userId);
        System.out.println("[ЗАДАЧИ СГЕНЕРИРОВАНЫ]");
    }
}
