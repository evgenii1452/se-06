package ru.kozlov.tm.Command.Basic;

import ru.kozlov.tm.Command.AbstractCommand;
import ru.kozlov.tm.Const.Command;
import ru.kozlov.tm.Enum.RoleType;

import java.util.Map;

public class HelpCommand extends AbstractCommand {
    @Override
    public RoleType[] roleTypes() {
        return new RoleType[] {null, RoleType.ADMIN, RoleType.CUSTOMER};
    }

    @Override
    public String getName() {
        return Command.HELP;
    }

    @Override
    public String getDescription() {
        return "Show all commands.";
    }

    @Override
    public void execute() {
        for (Map.Entry<String, AbstractCommand> commandEntry: bootstrap.getCommandMap().entrySet()){
            System.out.println(commandEntry.getKey() + ": " + commandEntry.getValue().getDescription());
        }
    }
}
