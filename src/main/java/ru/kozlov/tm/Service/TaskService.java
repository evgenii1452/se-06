package ru.kozlov.tm.Service;

import ru.kozlov.tm.Entity.Task;
import ru.kozlov.tm.Repository.TaskRepository;
import ru.kozlov.tm.Util.HelpUtil;

import java.text.ParseException;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

public class TaskService {
    private final TaskRepository taskRepository;

    public TaskService(final TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public String createTask(
            final String userId,
            final String projectId,
            final String taskName,
            final String taskDescription,
            final String dateStartAsString,
            final String dateEndAsString
    ) throws ParseException {
        stringValidation(projectId);
        stringValidation(taskName);
        stringValidation(taskDescription);
        stringValidation(dateStartAsString);
        stringValidation(dateEndAsString);

        final String taskId = UUID.randomUUID().toString();
        final Date dateStart = HelpUtil.convertStringToDate(dateStartAsString);
        final Date dateEnd = HelpUtil.convertStringToDate(dateEndAsString);

        final Task task = new Task();
        task.setId(taskId);
        task.setName(taskName);
        task.setDescription(taskDescription);
        task.setDateStart(dateStart);
        task.setDateEnd(dateEnd);
        task.setProjectId(projectId);
        task.setUserId(userId);

        return taskRepository.persist(task);
    }

    public void updateTask(
            final String projectId,
            final String taskId,
            final String name,
            final String description,
            final String dateStartAsString,
            final String dateEndAsString
    ) throws ParseException {
        stringValidation(taskId);
        stringValidation(name);
        stringValidation(description);
        stringValidation(dateStartAsString);
        stringValidation(dateEndAsString);
        stringValidation(taskId);

        final Date dateStart = HelpUtil.convertStringToDate(dateStartAsString);
        final Date dateEnd = HelpUtil.convertStringToDate(dateEndAsString);

        final Task task = new Task();
        task.setId(taskId);
        task.setName(name);
        task.setDescription(description);
        task.setDateStart(dateStart);
        task.setDateEnd(dateEnd);
        task.setProjectId(projectId);

        taskRepository.merge(task);
    }

    public Map<String, Task> getTasksByProjectId(final String userId, final String projectId) {
        stringValidation(projectId);

        return taskRepository.findAll(userId, projectId);
    }

    public void removeTaskById(final String userId, final String taskId) {
        stringValidation(taskId);

        final Task removedTask = taskRepository.remove(userId, taskId);

        if (removedTask == null) {
            throw new IllegalArgumentException("[ЗАДАЧА НЕ НАЙДЕНА]");
        }
    }

    public void removeTasksByProjectId(final String userId, final String projectId) {
        stringValidation(projectId);

        taskRepository.removeAll(userId, projectId);
    }

    private void stringValidation(final String value) {
        if (HelpUtil.stringIsEmpty(value)) {
            throw new NullPointerException("[ПОЛЯ НЕ МОГУТ БЫТЬ ПУСТЫМИ]");
        }
    }
}
