package ru.kozlov.tm.Controller;

import ru.kozlov.tm.Entity.Project;
import ru.kozlov.tm.Service.ProjectService;

import java.text.ParseException;
import java.util.List;

public class ProjectController {

    private final ProjectService projectService;

    public ProjectController(final ProjectService projectService) {
        this.projectService = projectService;
    }

    public void showAll(final String userId) {
        System.out.println("[ВАШИ ПРОЕКТЫ]");

        final List<Project> projectList = projectService.getAllProjects(userId);

        for (Project project: projectList) {
            System.out.println(project);
        }

        System.out.println("[OK]");
    }

    public void create(
            final String userId,
            final String name,
            final String description,
            final String dateStart,
            final String dateEnd
    ) {
        try {
            String projectId = projectService.createProject(userId, name, description, dateStart, dateEnd);
            System.out.println("[ПРОЕКТ " + projectId + " СОЗДАН]");
        } catch (IllegalArgumentException | NullPointerException | ParseException e) {
            System.out.println(e.getMessage());
        }
    }

    public void update(
            final String projectId,
            final String name,
            final String description,
            final String dateStart,
            final String dateEnd,
            final String userId
    ) {

        try {
            projectService.updateProject(projectId, name, description, dateStart, dateEnd, userId);
            System.out.println("[ПРОЕКТ " + projectId + " ИЗМЕНЕН]");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void remove(final String userId, final String projectId) {
        try {
            projectService.removeProjectById(userId, projectId);
        } catch (Exception e) {
            System.out.println(e.getMessage());

            return;
        }


        System.out.println("[ПРОЕКТ #" + projectId + " УДАЛЕН]");
    }

    public void removeAll(final String userId) {
        projectService.removeAllProjects(userId);
    }

    public void generate(final String userId) {
        int count = 1 + (int) (Math.random() * 8);

        for (int i = 1; i <= count; i++) {
            String name = "Проект #" + i;
            String description = "Описание проекта #" + i;
            String dateStart = "30/12/1900";
            String dateEnd = "30/12/1900";

            try {
                projectService.createProject(userId, name, description, dateStart, dateEnd);
            } catch (IllegalArgumentException | NullPointerException | ParseException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
