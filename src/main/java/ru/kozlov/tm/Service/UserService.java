package ru.kozlov.tm.Service;

import ru.kozlov.tm.Entity.User;
import ru.kozlov.tm.Enum.RoleType;
import ru.kozlov.tm.Repository.UserRepository;
import ru.kozlov.tm.Util.HelpUtil;
import ru.kozlov.tm.Util.PasswordHashUtil;

import java.nio.file.AccessDeniedException;
import java.util.UUID;

public class UserService {
    private final UserRepository userRepository;
    private ProjectService projectService;

    public UserService(
            final UserRepository userRepository,
            final ProjectService projectService
    ) {
        this.userRepository = userRepository;
        this.projectService = projectService;
    }

    public User getUserById(final String id) {
        return userRepository.getById(id);
    }

    public User getUserByLogin(final String login) throws Exception {
        stringValidation(login);

        final User user = userRepository.findByLogin(login);

        if (user == null) {
            throw new Exception("[ПОЛЬЗОВАТЕЛЬ НЕ НАЙДЕН]");
        }

        return user;
    }

    public void create(final String login, final String password) throws Exception {
        stringValidation(login);
        stringValidation(password);

        if (userRepository.loginExists(login)) {
            throw new Exception("[ЛОГИН УЖЕ ЗАНЯТ]");
        }

        final User user = new User();
        user.setId(UUID.randomUUID().toString());
        user.setLogin(login);
        user.setPassword(PasswordHashUtil.md5(password));
        user.setRole(RoleType.CUSTOMER);

        userRepository.persist(user);
    }

    public User authentication(final String login, final String password) throws Exception {
        stringValidation(login);
        stringValidation(password);

        final User user = getUserByLogin(login);
        String md5Password = PasswordHashUtil.md5(password);

        if (user == null) {
            throw new Exception("[ПОЛЬЗОВАТЕЛЬ НЕ НАЙДЕН]");
        }

        if (!user.getPassword().equals(md5Password)) {
            throw new AccessDeniedException("[ВВЕДЕН НЕВЕРНЫЙ ПАРОЛЬ]");
        }

        return user;
    }

    public void updateUser(
            final String id,
            final String newLogin,
            final String newPassword
    ) {
        stringValidation(id);
        stringValidation(newLogin);
        stringValidation(newPassword);

        final User user = getUserById(id);
        user.setLogin(newLogin);
        user.setPassword(PasswordHashUtil.md5(newPassword));

        userRepository.merge(user);
    }

    public void updatePassword(final User user, final String newPassword) {
        stringValidation(newPassword);

        user.setPassword(PasswordHashUtil.md5(newPassword));
        userRepository.merge(user);
    }

    private void stringValidation(final String value) {
        if (HelpUtil.stringIsEmpty(value)) {
            throw new NullPointerException("[ВСЕ ПОЛЯ ДОЛЖНЫ БЫТЬ ЗАПОЛНЕНЫ]");
        }
    }

    public boolean userHasProject(final String userId, final String projectId) {
        return projectService.isProjectBelongsToUser(projectId, userId);
    }
}
