package ru.kozlov.tm.Command.Task;

import ru.kozlov.tm.Command.AbstractCommand;
import ru.kozlov.tm.Const.Command;
import ru.kozlov.tm.Enum.RoleType;

public class TaskRemoveCommand extends AbstractCommand {
    @Override
    public RoleType[] roleTypes() {
        return new RoleType[] {RoleType.ADMIN, RoleType.CUSTOMER};
    }

    @Override
    public String getName() {
        return Command.TASK_REMOVE;
    }

    @Override
    public String getDescription() {
        return "Remove selected task";
    }

    @Override
    public void execute() {
        String userId = bootstrap.getUserId();

        System.out.println("Введите id задачи:");
        final String id = bootstrap.getScanner().nextLine();

        bootstrap.getTaskController().remove(userId, id);
    }
}
