package ru.kozlov.tm.Repository;

import ru.kozlov.tm.Entity.Project;

import java.util.*;

public class ProjectRepository {
    private final Map<String, Project> projects = new HashMap<>();

    public void removeAll(final String userId) {
        Iterator<Map.Entry<String, Project>> it = projects.entrySet().iterator();

        while (it.hasNext()){
            Map.Entry<String, Project> projectEntry = it.next();

            if (userId.equals(projectEntry.getValue().getId())) {
                it.remove();
            }
        }
    }

    public String persist(final Project project) {
        projects.put(project.getId() , project);

        return project.getId();
    }

    public List<Project> findAll(final String userId) {
        List<Project> projectList = new ArrayList<>();

        for (Map.Entry<String, Project> projectMap: projects.entrySet()) {
            if (userId.equals(projectMap.getValue().getUserId())) {
                projectList.add(projectMap.getValue());
            }
        }
        return projectList;
    }

    public Project remove(final String id) {
        return projects.remove(id);
    }

    public Project find(final String id) {
        return projects.get(id);
    }

    public void merge(Project project) {
        if (projects.containsKey(project.getId())) {
            projects.replace(project.getId(), project);
        }

        if (!projects.containsKey(project.getId())){
            projects.put(project.getId(), project);
        }
    }

    public boolean keyExists(final String key) {
        return projects.containsKey(key);
    }
}
