package ru.kozlov.tm.Command.Project;

import ru.kozlov.tm.Command.AbstractCommand;
import ru.kozlov.tm.Const.Command;
import ru.kozlov.tm.Enum.RoleType;

public class ProjectCreateCommand extends AbstractCommand {
    @Override
    public RoleType[] roleTypes() {
        return new RoleType[] {RoleType.ADMIN, RoleType.CUSTOMER};
    }

    @Override
    public String getName() {
        return Command.PROJECT_CREATE;
    }

    @Override
    public String getDescription() {
        return "Create new project";
    }

    @Override
    public void execute() {
        final String userId = bootstrap.getUserId();

        System.out.println("Введите название проекта:");
        final String name = bootstrap.getScanner().nextLine();

        System.out.println("Введите описание проекта:");
        final String description = bootstrap.getScanner().nextLine();

        System.out.println("Введите дату начала проекта в формате \"30/12/1900\"");
        final String dateStart = bootstrap.getScanner().nextLine();

        System.out.println("Введите дату завершения проекта в формате \"30/12/1900\"");
        final String dateEnd = bootstrap.getScanner().nextLine();

        bootstrap.getProjectController().create(userId, name, description, dateStart, dateEnd);
    }
}
