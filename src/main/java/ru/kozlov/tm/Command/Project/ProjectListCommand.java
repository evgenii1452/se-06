package ru.kozlov.tm.Command.Project;

import ru.kozlov.tm.Command.AbstractCommand;
import ru.kozlov.tm.Const.Command;
import ru.kozlov.tm.Enum.RoleType;

public class ProjectListCommand extends AbstractCommand {
    @Override
    public RoleType[] roleTypes() {
        return new RoleType[] {RoleType.ADMIN, RoleType.CUSTOMER};
    }

    @Override
    public String getName() {
        return Command.PROJECT_LIST;
    }

    @Override
    public String getDescription() {
        return "Show all projects";
    }

    @Override
    public void execute() {
        final String userId = bootstrap.getUserId();

        bootstrap.getProjectController().showAll(userId);
    }
}
