package ru.kozlov.tm.Command.Project;

import ru.kozlov.tm.Command.AbstractCommand;
import ru.kozlov.tm.Const.Command;
import ru.kozlov.tm.Enum.RoleType;

public class ProjectGenerateCommand extends AbstractCommand {
    @Override
    public RoleType[] roleTypes() {
        return new RoleType[] {RoleType.ADMIN, RoleType.CUSTOMER};
    }

    @Override
    public String getName() {
        return Command.PROJECT_GENERATE;
    }

    @Override
    public String getDescription() {
        return "Project generation";
    }

    @Override
    public void execute() {
        final String userId = bootstrap.getUserId();
        bootstrap.getProjectController().generate(userId);
        System.out.println("[ПРОЕКТЫ СГЕНЕРИРОВАНЫ]");
    }
}
