package ru.kozlov.tm.Util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class HelpUtil {

    public static Date convertStringToDate(final String string) throws ParseException {
        final DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        return dateFormat.parse(string);
    }

    public static boolean stringIsEmpty(final String string) {
            return string == null || string.trim().isEmpty();
    }

}
