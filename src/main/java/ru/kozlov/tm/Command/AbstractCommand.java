package ru.kozlov.tm.Command;

import ru.kozlov.tm.Bootstrap.Bootstrap;
import ru.kozlov.tm.Enum.RoleType;

public abstract class AbstractCommand {
    protected Bootstrap bootstrap;

    public void setBootstrap(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public abstract RoleType[] roleTypes();
    public abstract String getName();
    public abstract String getDescription();
    public abstract void execute();

//    public boolean authorize() {
//        RoleType userRole = bootstrap.getCurrentUser().getRole();
//
//        for (RoleType roleType: roleTypes()){
//            if (roleType.equals(userRole)) {
//                return true;
//            }
//        }
//
//        return false;
//    };
}
