package ru.kozlov.tm;

import ru.kozlov.tm.Bootstrap.Bootstrap;
import ru.kozlov.tm.Command.*;
import ru.kozlov.tm.Command.Basic.ExitCommand;
import ru.kozlov.tm.Command.Basic.HelpCommand;
import ru.kozlov.tm.Command.Project.*;
import ru.kozlov.tm.Command.Task.*;
import ru.kozlov.tm.Command.User.*;

public class App {

    public static void main(String[] args) throws Exception {
        final AbstractCommand[] commands = {
                new ProjectClearCommand(),
                new ProjectCreateCommand(),
                new ProjectListCommand(),
                new ProjectRemoveCommand(),
                new ProjectUpdateCommand(),
                new ProjectGenerateCommand(),
                new TaskClearCommand(),
                new TaskCreateCommand(),
                new TaskListCommand(),
                new TaskRemoveCommand(),
                new TaskUpdateCommand(),
                new TaskGenerateCommand(),
                new UserInfoCommand(),
                new UserLoginCommand(),
                new UserRegistrationCommand(),
                new UserLogoutCommand(),
                new UserUpdateCommand(),
                new UserUpdatePasswordCommand(),
                new HelpCommand(),
                new ExitCommand(),
        };

        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init(commands);
    }
}
