package ru.kozlov.tm.Command.Project;

import ru.kozlov.tm.Command.AbstractCommand;
import ru.kozlov.tm.Const.Command;
import ru.kozlov.tm.Enum.RoleType;

public class ProjectUpdateCommand extends AbstractCommand {
    @Override
    public RoleType[] roleTypes() {
        return new RoleType[] {RoleType.ADMIN, RoleType.CUSTOMER};
    }

    @Override
    public String getName() {
        return Command.PROJECT_UPDATE;
    }

    @Override
    public String getDescription() {
        return "Update project";
    }

    @Override
    public void execute() {
        final String userId = bootstrap.getUserId();
        bootstrap.getProjectController().showAll(userId);

        System.out.println("Чтобы пропусть поле нажмите Enter");
        System.out.println("Введите id проекта:");
        final String projectId = bootstrap.getScanner().nextLine();

        System.out.println("Введите новое название проекта:");
        final String name = bootstrap.getScanner().nextLine();

        System.out.println("Введите новое описание проекта:");
        final String description = bootstrap.getScanner().nextLine();

        System.out.println("Введите новую дату начала проекта в формате \"30/12/1900\"");
        final String dateStart = bootstrap.getScanner().nextLine();

        System.out.println("Введите новую дату завершения проекта в формате \"30/12/1900\"");
        final String dateEnd = bootstrap.getScanner().nextLine();
        
        bootstrap.getProjectController().update(projectId, name, description, dateStart, dateEnd, userId);
    }
}
