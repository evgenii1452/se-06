package ru.kozlov.tm.Repository;

import ru.kozlov.tm.Entity.Task;

import java.util.*;

public class TaskRepository {
    private final Map<String, Task> tasks = new HashMap<>();


    public String persist(final Task task) {
        tasks.put(task.getId(), task);

        return task.getId();
    }

    public Map<String, Task> findAll(final String userId, final String projectId) {
        Map<String, Task> projectTasks = new HashMap<>();

        for (Map.Entry<String, Task> taskEntry : tasks.entrySet()) {
            Task task = taskEntry.getValue();

            if (projectId.equals(task.getProjectId()) && userId.equals(task.getUserId())) {
                projectTasks.put(task.getId(), task);
            }
        }

        return projectTasks;
    }

    public Task remove(final String userId, final String id) {
        Iterator<Map.Entry<String, Task>> it = tasks.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry<String, Task> taskEntry = it.next();
            Task task = taskEntry.getValue();

            if (id.equals(task.getId()) && userId.equals(task.getUserId())) {
                it.remove();

                return task;
            }
        }

        return null;
    }

    public void removeAll(
            final String userId,
            final String projectId
    ) {
        Iterator<Map.Entry<String, Task>> it = tasks.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry<String, Task> taskEntry = it.next();

            if (taskEntry.getKey().equals(projectId) && userId.equals(taskEntry.getValue().getUserId())) {
                it.remove();
            }
        }
    }

    public Task find(final String userId, final String id) {
        for (Map.Entry<String, Task> taskEntry : tasks.entrySet()) {
            Task task = taskEntry.getValue();

            if (id.equals(task.getId()) && userId.equals(task.getUserId())) {
                return task;
            }
        }

        return null;
    }

    public void merge(final Task task) {
        if (tasks.containsKey(task.getId())) {
            tasks.replace(task.getId(), task);
        }

        if (!tasks.containsKey(task.getId())){
            tasks.put(task.getId(), task);
        }
    }

    public boolean keyExists(final String key) {
        return tasks.containsKey(key);
    }
}
