package ru.kozlov.tm.Controller;

import ru.kozlov.tm.Entity.Project;
import ru.kozlov.tm.Entity.Task;
import ru.kozlov.tm.Service.ProjectService;
import ru.kozlov.tm.Service.TaskService;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

public class TaskController {

    private final TaskService taskService;
    private final ProjectService projectService;

    public TaskController(
            final TaskService taskService,
            final ProjectService projectService
    ) {
        this.taskService = taskService;
        this.projectService = projectService;
    }

    public void create(
            final String userId,
            final String projectId,
            final String name,
            final String description,
            final String dateStart,
            final String dateEnd
    ) {
        try {
            String taskId = taskService.createTask(userId, projectId, name, description, dateStart, dateEnd);
            System.out.println("[ЗАДАЧА \"" + taskId + "\" СОЗДАНА]");
        } catch (NullPointerException | IllegalArgumentException | ParseException e) {
            System.out.println(e.getMessage());
        }
    }

    public void update(
            final String userId,
            final String projectId,
            final String taskId,
            final String name,
            final String description,
            final String dateStart,
            final String dateEnd
    ) {
        try {
            taskService.updateTask(projectId, taskId, name, description, dateStart, dateEnd);
            System.out.println("[ЗАДАЧА " + taskId + " ИЗМЕНЕНА]");
        } catch (IllegalArgumentException | NullPointerException | ParseException e) {
            System.out.println(e.getMessage());
        }

    }

    public void showAll(
            final String userId,
            final String projectId
    ) {
        try {
            Map<String, Task> tasks = taskService.getTasksByProjectId(userId, projectId);

            System.out.println("Кол-во задач:" + tasks.size());

            for (Map.Entry<String, Task> taskEntry : tasks.entrySet()) {
                System.out.println(taskEntry.getValue());
            }
        } catch (IllegalArgumentException | NullPointerException e) {
            System.out.println(e.getMessage());
        }
    }

    public void remove(
            final String userId,
            final String taskId
    ) {
        try {
            taskService.removeTaskById(userId, taskId);
        } catch (IllegalArgumentException | NullPointerException e) {
            System.out.println(e.getMessage());

            return;
        }

        System.out.println("[ЗАДАЧА #" + taskId + " УДАЛЕНА]");
    }

    public void removeAll(
            final String userId,
            final String projectId
    ) {
        try {
            taskService.removeTasksByProjectId(userId, projectId);
        } catch (IllegalArgumentException | NullPointerException e) {
            System.out.println(e.getMessage());

            return;
        }

        taskService.removeTasksByProjectId(userId, projectId);

        System.out.println("[ЗАДАЧИ ПРОЕКТА #" + projectId + " УДАЛЕНЫ]");
    }

    public void generate(final String userId) {
        List<Project> projectList = projectService.getAllProjects(userId);

        for (Project project : projectList) {
            int count = 1 + (int) (Math.random() * 8);

            for (int i = 1; i <= count; i++) {
                final String taskName = "Задача #" + i;
                final String taskDescription = "Описание задачи #" + i;
                final String dateStart = "30/12/1900";
                final String dateEnd = "30/12/1900";

                try {
                    taskService.createTask(userId, project.getId(), taskName, taskDescription, dateStart, dateEnd);
                } catch (NullPointerException | IllegalArgumentException | ParseException e) {
                    System.out.println(e.getMessage());
                }
            }
        }
    }

}
