package ru.kozlov.tm.Entity;

import ru.kozlov.tm.Enum.RoleType;

public class User {
    private String id;
    private String login;
    private String password;
    private RoleType role;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public RoleType getRole() {
        return role;
    }

    public void setRole(RoleType role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "id: " + id + "\n" +
                "login: " + login + "\n" +
                "role: " + role + "\n";
    }
}
