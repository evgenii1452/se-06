package ru.kozlov.tm.Command.User;

import ru.kozlov.tm.Command.AbstractCommand;
import ru.kozlov.tm.Entity.User;
import ru.kozlov.tm.Enum.RoleType;

public class UserInfoCommand extends AbstractCommand {
    @Override
    public RoleType[] roleTypes() {
        return new RoleType[] {RoleType.ADMIN, RoleType.CUSTOMER, null};
    }

    @Override
    public String getName() {
        return "user-info";
    }

    @Override
    public String getDescription() {
        return "Show information about the current user";
    }

    @Override
    public void execute() {
        User user = bootstrap.getCurrentUser();

        if (user.getRole() == null) {
            System.out.println("[НЕАВТОРИЗОВАННЫЙ ПОЛЬЗОВАТЕЛЬ]");

            return;
        }

        System.out.println(user.toString());
    }
}
