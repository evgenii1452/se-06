package ru.kozlov.tm.Command.Task;

import ru.kozlov.tm.Command.AbstractCommand;
import ru.kozlov.tm.Const.Command;
import ru.kozlov.tm.Enum.RoleType;

public class TaskClearCommand extends AbstractCommand {
    @Override
    public RoleType[] roleTypes() {
        return new RoleType[] {RoleType.ADMIN, RoleType.CUSTOMER};
    }

    @Override
    public String getName() {
        return Command.TASK_CLEAR;
    }

    @Override
    public String getDescription() {
        return "Remove all tasks";
    }

    @Override
    public void execute() {
        String userId = bootstrap.getUserId();
        System.out.println("Введите id проекта:");
        final String projectId = bootstrap.getScanner().nextLine();

        bootstrap.getTaskController().removeAll(userId, projectId);
    }
}
