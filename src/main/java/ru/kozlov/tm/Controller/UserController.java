package ru.kozlov.tm.Controller;

import ru.kozlov.tm.Entity.User;
import ru.kozlov.tm.Service.UserService;


public class UserController {
    final private UserService userService;

    public UserController(final UserService userService) {
        this.userService = userService;
    }

    public User authentication(final String login, final String password) {
        try {
            return userService.authentication(login, password);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return null;
    }

    public void registration(String login, String password) {
        try {
            userService.create(login, password);
            System.out.println("[РЕГИСТРАЦИЯ ПРОШЛА УСПЕШНО]");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void updatePassword(
            final String login,
            final String currentPassword,
            final String newPassword
    ) {
        try {
            final User user = userService.authentication(login, currentPassword);
            userService.updatePassword(user, newPassword);
            System.out.println("[ПАРОЛЬ ОБНОВЛЕН]");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }


    public void update(
            final String id,
            final String newLogin,
            final String newPassword
    ) {
        userService.updateUser(id, newLogin, newPassword);
    }
}
