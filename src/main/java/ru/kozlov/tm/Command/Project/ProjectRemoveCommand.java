package ru.kozlov.tm.Command.Project;

import ru.kozlov.tm.Command.AbstractCommand;
import ru.kozlov.tm.Const.Command;
import ru.kozlov.tm.Enum.RoleType;

public class ProjectRemoveCommand extends AbstractCommand {
    @Override
    public RoleType[] roleTypes() {
        return new RoleType[] {RoleType.ADMIN, RoleType.CUSTOMER};
    }

    @Override
    public String getName() {
        return Command.PROJECT_REMOVE;
    }

    @Override
    public String getDescription() {
        return "Remove selected project.";
    }

    @Override
    public void execute() {
        final String userId = bootstrap.getUserId();
        bootstrap.getProjectController().showAll(userId);

        System.out.println("Введите id проекта:");
        final String projectId = bootstrap.getScanner().nextLine();

        bootstrap.getProjectController().remove(userId, projectId);
    }
}
