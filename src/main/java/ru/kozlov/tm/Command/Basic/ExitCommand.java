package ru.kozlov.tm.Command.Basic;

import ru.kozlov.tm.Command.AbstractCommand;
import ru.kozlov.tm.Const.Command;
import ru.kozlov.tm.Enum.RoleType;

public class ExitCommand extends AbstractCommand {
    @Override
    public RoleType[] roleTypes() {
        return new RoleType[] {null, RoleType.ADMIN, RoleType.CUSTOMER};
    }

    @Override
    public String getName() {
        return Command.EXIT;
    }

    @Override
    public String getDescription() {
        return "Exit task manager";
    }

    @Override
    public void execute() {
        System.out.println("*** GOODBYE ***");
        System.exit(0);
    }
}
